class Bird extends Graphics{
	constructor(){
		super()
		this.width  = 30
		this.height = 30
		this.x      = 40
		this.y      = 150
		this.speed  = 0
	}

	draw(){
		this.ctx.beginPath()

		var img = new Image()
		img.src = 'img/cito.png'

		img.addEventListener('load',()=>{
			this.ctx.drawImage(img, this.x, this.y, this.width, this.height)
		})

		this.ctx.closePath()
	}
}