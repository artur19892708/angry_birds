class Graphics{
	constructor(){
		this.canvas = document.querySelector("canvas")

		this.ctx    = this.canvas.getContext('2d')
		
	}

	rand(limit){
		return Math.round(Math.random()*limit)
	}
}