class Wall extends Graphics{
	constructor(){
		super()
		this.color  = '#73BF2E'
		this.width  = 50
		this.height = this.rand(this.canvas.height-200)
		this.y = 0
		this.x = this.canvas.width
	}

	draw(){
		this.ctx.beginPath()

		this.ctx.fillStroke = 'blue'
		this.ctx.lineWidth = 5;
		this.ctx.fillStyle = this.color;
		this.ctx.fillRect(this.x, this.y, this.width, this.height)
		this.ctx.fillRect(this.x,this.height+200,this.width, this.canvas.height-this.height-200)

		this.ctx.closePath()
	}

}